module.exports =  function(api) {
  const presets = [];

  if(api.env('test')){
    presets.push(["@babel/env", {
      targets: {
        node: 'current',
      }
    }]);
  } else {
    api.cache(false);
    presets.push(["@babel/env", {
      targets: {
        node: 'current'
      },
      modules: process.env.ES6_MODULES ? false : "cjs",
    }]);
  }

  const plugins = [
    ["@babel/proposal-throw-expressions"]
  ];

  let ignore = [];
  if (process.env.NODE_ENV !== 'test') {
    ignore = ignore.concat([
      /__tests__/,
      /__integrations__/,
      /__mocks__/,
      /__jsonSchemas__/
    ]);
  }


  return {
    presets,
    plugins,
    ignore,
    sourceMaps: "inline"
  };
};

