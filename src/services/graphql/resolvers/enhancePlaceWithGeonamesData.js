/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {geonamesClient} from "../../api/geonamesClient";

/**
 * @param {Model} place
 * @param {SynaptixDatastoreSession} synaptixSession
 * @return {Model}
 */
export let enhancePlaceWithGeonamesData = async ({place, synaptixSession}) => {
  let id;

  // If place id is an absolute URI, ID is just after the origin URI.
  if (!!place.id.match(/https?:\/\/www.geonames.org\//)) {
    id = place.id.replace(/https?:\/\/www.geonames.org\/([^\/]*).*/, "$1");
    // If place id is a prefixed URI, just remove the prefix.
  } else {
    id = place.id.slice(place.id.indexOf(":") + 1);
  }

  let geonamesPlace = await geonamesClient.getPlaceById({id, lang: synaptixSession.getContext().getLang()});
  return Object.assign({}, geonamesPlace, place);
};