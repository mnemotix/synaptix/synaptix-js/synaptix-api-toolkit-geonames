/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let gotMockJSON = jest.fn();
let gotMock = jest.fn(() => ({
  json: gotMockJSON
}));

jest.doMock('got', () => gotMock);
let geonamesClient = require('../geonamesClient').geonamesClient;

/**
 * Setup process.env.GEONAMES_USERNAME for the test suite, without side effects
 */
let processEnvGeonamesUserSave;
beforeAll(() => {
  processEnvGeonamesUserSave = process.env.GEONAMES_USERNAME;
  process.env.GEONAMES_USERNAME = 'MyGeonamesUser';
});
afterAll(() => {
  process.env.GEONAMES_USERNAME = processEnvGeonamesUserSave;
});

describe('searchPlaces()', () => {
  test('regular call', async () => {
    gotMockJSON.mockImplementation(() => {
      return ({
        geonames: [{
          geonameId: 'place123',
          countryId: 'country456'
        }]
      })
    });
    let results = await geonamesClient.searchPlaces({
      lang: 'fr', 
      qs: 'myCity',
      limit: 5
    });

    expect(gotMock.mock.calls[0][0]).toEqual( "http://api.geonames.org/searchJSON?name_startsWith=myCity&lang=fr&searchlang=fr&maxRows=5&featureCode=PCLI&featureCode=PCLD&featureCode=ADM1&featureCode=ADM3&featureCode=ADM4&featureCode=ADM5&featureCode=PPL&featureCode=PPLA&featureCode=PPLA2&featureCode=PPLA3&featureCode=PPLA4&featureCode=PPLA5&username=MyGeonamesUser");
  });

  test('default values for parameters', async () => {
    gotMockJSON.mockImplementation(() => ({
      geonames: [{
        geonameId: 'place123',
        countryId: 'country456'
      }]
    }));
    let results = await geonamesClient.searchPlaces({
      qs: 'myCity'
    });

    expect(gotMock.mock.calls[0][0]).toEqual("http://api.geonames.org/searchJSON?name_startsWith=myCity&lang=fr&searchlang=fr&maxRows=10&featureCode=PCLI&featureCode=PCLD&featureCode=ADM1&featureCode=ADM3&featureCode=ADM4&featureCode=ADM5&featureCode=PPL&featureCode=PPLA&featureCode=PPLA2&featureCode=PPLA3&featureCode=PPLA4&featureCode=PPLA5&username=MyGeonamesUser");
  });
});
