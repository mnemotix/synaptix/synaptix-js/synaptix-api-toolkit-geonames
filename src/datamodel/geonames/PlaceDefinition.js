/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  LabelDefinition,
  LiteralDefinition,
  MnxOntologies,
  ModelDefinitionAbstract,
} from "@mnemotix/synaptix.js";
import {PlaceGraphQLDefinition} from "./graphql/PlaceGraphQLDefinition";

export class PlaceDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return PlaceGraphQLDefinition;
  }

  static isInstantiable(){
    return false;
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions(){
    return [
      MnxOntologies.mnxGeo.ModelDefinitions.SpatialEntityDefinition,
      MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition
    ];
  }

  static getRdfInstanceBaseUri({nodesNamespaceURI, nodeTypeFormatter}) {
    return "https://www.geonames.org";
  }

  static isMatchingURI({uri, nodesNamespaceURI, nodeTypeFormatter}){
    return !!uri.match(/http?s:\/\/(www|sws)\.geonames\.org/);
  }

  static getRdfType(){
    return "http://www.geonames.org/ontology#GeonamesFeature";
  }

  static getGraphQLType(){
    return "Place";
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: "name",
        pathInIndex: "alternateNames", // @see http://www.geonames.org/export/web-services.html#get
        rdfDataProperty: "skos:prefLabel"
      }),
      // new LabelDefinition({
      //   labelName: 'color',
      //   rdfDataProperty: "mnx:color"
      // })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    let heritedLiterals = super.getLiterals();

    return [
      ...heritedLiterals,
      new LiteralDefinition({
        literalName: 'bbox',
        pathInIndex: 'bbox',
      }),
      new LiteralDefinition({
        literalName: 'lng',
        pathInIndex: 'lng',
      }),
      new LiteralDefinition({
        literalName: 'lat',
        pathInIndex: 'lat',
        rdfDataType: "http://www.w3.org/2001/XMLSchema#float",
      }),
      new LiteralDefinition({
        literalName: 'wikipediaURL',
        pathInIndex: 'wikipediaURL'
      }),
      new LiteralDefinition({
        literalName: 'countryCode',
        pathInIndex: 'countryCode'
      })
    ];
  }
}

