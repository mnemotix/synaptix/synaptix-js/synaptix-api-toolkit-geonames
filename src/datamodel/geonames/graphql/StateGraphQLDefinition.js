import {GraphQLInterfaceDefinition} from '@mnemotix/synaptix.js';

export class StateGraphQLDefinition extends GraphQLInterfaceDefinition {
  /**
   * @inheritDoc
   */
  static getExtraResolvers(){
    return {
      State: {
        stateCode: (place) => place.adminCode1
      }
    }
  }
}
