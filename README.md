# Synaptix Api Toolkit Geonames

This is the Synaptix API toolkit Geonames extension.

[![npm version](https://badge.fury.io/js/%40mnemotix%2Fsynaptix-api-toolkit-geonames.svg)](https://badge.fury.io/js/%40mnemotix%2Fsynaptix-js/synaptix-api-toolkit-geonames)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![pipeline status](https://gitlab.com/mnemotix/synaptix-js/synaptix-api-toolkit-geonames/badges/master/pipeline.svg)](https://gitlab.com/mnemotix/synaptix-js/synaptix-api-toolkit-geonames/commits/master)
[![coverage report](https://gitlab.com/mnemotix/synaptix-js/synaptix-api-toolkit-geonames/badges/master/coverage.svg)](https://gitlab.com/mnemotix/synaptix-js/synaptix-api-toolkit-geonames/commits/master)


# use it with yarn berry

> yarn set verion berry

# to link it in another project
> yarn link "path/to/synaptix-api-toolkit" 

# to release a new version : 
> yarn version patch
> yarn release
